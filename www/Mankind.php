<?php
/*
Implementujte třídu Mankind (lidstvo), která pracuje s instancemi třídy Osoba.

Obecné požadavky:
- v jednom okamžiku může existovat pouze jedna instance třídy (marťané nejsou lidstvo...)
- s třídou musí být možné pracovat jako s polem (klíče jsou ID osob) a procházet ji foreach cyklem

Další požadované operace:
- načtení hodnot ze vstupního souboru (upřesnění níže)
- získání osoby dle ID
- získání procentuálního zastoupení mužů v lidstvu



Nacteni hodnoty ze vstupniho souboru

Vstupni soubor obsahuje seznam osob. Kazda osoba je na samostatnem
radku. Kazdy radek obsahuje ciselne id osoby, jmeno, prijmeni,
pohlavi (M/Z) a datum narozeni ve formatu dd.mm.yyyy. Oddelovač údajů
o osobě je střednik. Kódování souboru je UTF-8 

Priklad:
123;Michal;Nový;M;01.11.1962
3457;Petra;Veverková;Z;13.4.1887

Predpokladany pocet osob v souboru je <= 1000.

Dále uveďte, jak byste řešili situaci, kdy soubor bude řádově větší
(např. stovky MB) a to jak z hlediska této metody, tak celé třídy.

*/

final class Mankind implements Iterator {
    private $people = array();
    private static $instance = null;

    private function Mankind () {}

    private function __clone () {}
    
    public static function Instance () {
        if ($this->instance === null) {
            $this->instance = new Mankind();
        }
        return $this->instance;
    }

    public function rewind() {
        reset($this->people);
    }
  
    public function current() {
        $person = current($this->people);
        return $person;
    }
  
    public function key() {
        $id = key($this->people);
        return $id;
    }
  
    public function next() {
        $person = next($this->people);
        return $person;
    }
  
    public function valid() {
        $id = key($this->people);
        return is_int($id);
    }

    public function addPerson ($data) {
        if (!$data && !is_array($data)) {
            throw new Exception('Dataset is empty or is not array.');
            return;
        }

        var $id = 1 * $data['id'];

        if (isset($this->people[$id])) {
            throw new Exception('Duplicate ID "' . $id . '" was not added.');
            return;
        }

        $this->people[$id] = new Person($data);
    }

    public function getPersonById ($id) {
        if (!is_numeric($id)) {
            throw new Exception('ID is only numbers.');
            return;
        }

        if (isset($this->people[$id])) {
            return $this->people[$id];
        }

        return NULL;
    }

    public function getNumberOfPeople ($byType, $value) {
        var $result = 0;

        foreach ($this->people as $id => $person) {
            if ($person->{$byType} === $value) {
                $result++;
            }
        }

        return $result;
    }

    public function getPercentageOfPeople ($byType, $value) {
        var $number = $this->getNumberOfPeople($byType, $value);
        var $numberOfPersons = count($this->people);

        return ($number / $numberOfPersons) * 100;
    }

    public function getPercentageOfPeopleBySex ($sex = 'M') {
        return $this->getPercentageOfPeople('sex', $sex);
    }

    public function setPeople ($data) {
        if (!$data && !is_array($data)) {
            throw new Exception('Dataset is empty or is not array.');
            return;
        }

        foreach ($data as $person) {
            $this->addPerson($person);
        }
    }

    public function setPeopleFromCsv ($url, $delimiter = ';') {
        var $columns = array('id', 'name', 'family', 'sex', 'birth');

        if (($file = fopen($url, "r")) !== FALSE) {
            while (($row = fgetcsv($file, 0, $delimiter)) !== FALSE) {
                var $data = array();
                
                foreach ($row as $key => $value) {
                    $data[$columns[$key]] = $value;
                }
                
                $this->addPerson($data);
                unset($data);
            }
            fclose($file);
        } else {
            throw new Exception('Cannot open file.');
            return;
        }
    }
}