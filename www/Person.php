<?php
/*
Implementujte třídu Person (osoba).

Osoba má tyto informace:
- jedinečné číselné ID
- jméno
- příjmení
- pohlaví M/Z
- datum narození
Tyto informace lze z instance třídy získat, ale nelze je již měnit (možnost změny pohlaví a přejmenování neuvažujeme).

Operace:
- získání délky života osoby ve dnech

*/

class Person {
    private $id;
    private $name;
    private $fname;
    private $sex;
    private $birth;

    public function Person ($data) {
        $this->setId($data['id']);
        $this->setName($data['name']);
        $this->setFname($data['family']);
        $this->setSex($data['sex']);
        $this->setBirth($data['birth']);
    }

    private function setId ($id) {
        $this->id = $id;
    }

    public function getId () {
        return $this->get('id');
    }

    private function setName ($name) {
        $this->name = $name;
    }

    public function getName () {
        return $this->get('name');
    }

    private function setFamily ($family) {
        $this->family = $family;
    }

    public function getFamily () {
        return $this->get('family');
    }

    private function setSex ($sex) {
        $this->sex = $sex;
    }

    public function getSex () {
        return $this->get('sex');
    }

    private function setBirth ($birth) {
        if (!$birth instanceof DateTime) {
            $birth = new DateTime($birth);
        }
        $this->birth = $birth;
    }

    public function getBirth ($birth) {
        return $this->get('birth');
    }

    public function get ($type) {
        return $this->{$type};
    }
}